﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    private Transform player;
    private float distFromPlayer;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        distFromPlayer = Vector3.Distance(player.transform.position, this.transform.position);
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.position = new Vector3(this.player.position.x, this.transform.position.y, this.player.position.z - distFromPlayer);
	}
}
