﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour {

    public Transform[] waypoints;
    public Transform eyes;
    private int waypointNumber = 0;

    private NavMeshAgent agent;
    private Animator anim;

    private float patrolSpeed = 1.5f;
    private float chaseSpeed = 5.0f;
    private float lookSphereRadius = 10.0f;
    private float maxLookDistance = 15.0f;
    private float maxAttackRange = 10.0f;
    private float castSpeed = 10.0f;

    private Transform playerTransform;
    public GameObject spellPrefab;

    public enum State
    {
        patrol,
        chase,
        attack
    }

    private State _state;

    private void Awake()
    {
        this.agent = GetComponent<NavMeshAgent>();
        this.anim = GetComponent<Animator>();
        this.playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        this._state = State.patrol;
    }

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        UpdateState(_state);

        DecideAction();
	}

    // Update to selected State
    void UpdateState(State state)
    {
        switch(state)
        {
            case State.patrol:
                Patrol();
                break;

            case State.chase:
                Chase();
                break;

            case State.attack:
                Attack();
                break;

            default:
                Patrol();
                break;
        }
    }

    // Deciding on Action
    void DecideAction()
    {
        Debug.DrawRay(this.eyes.position, this.transform.forward.normalized * this.maxLookDistance, Color.red);
        Debug.DrawRay(this.eyes.position, this.transform.forward.normalized * this.maxAttackRange, Color.green);

        RaycastHit hit;

        if (Physics.SphereCast(this.eyes.position, this.lookSphereRadius, this.transform.forward, out hit, this.maxAttackRange, 9)
            && hit.collider.CompareTag("Player"))
        {
            this._state = State.attack;
        } else if (Physics.SphereCast(this.eyes.position, this.lookSphereRadius, this.transform.forward, out hit, this.maxLookDistance, 9)
            && hit.collider.CompareTag("Player"))
        {
            this._state = State.chase;
        } else if(Vector3.Distance(this.transform.position, this.playerTransform.position) >= this.maxLookDistance)
        {
            this._state = State.patrol;
        }
    }

    // Actions
    void Patrol()
    {
        agent.destination = waypoints[this.waypointNumber].position;
        if (agent.remainingDistance <= agent.stoppingDistance && !agent.pathPending)
        {
            // this.waypointNumber = (this.waypointNumber + 1) % this.waypoints.Length;
            this.waypointNumber = Random.Range(0, this.waypoints.Length);
        }
        this.anim.SetFloat("moveSpeed", 1.0f);
    }

    void Chase()
    {
        this.anim.SetFloat("moveSpeed", 1.0f);
        agent.isStopped = false;
        agent.destination = this.playerTransform.position;
        agent.speed = this.chaseSpeed;
    }

    void Attack()
    {
        this.agent.isStopped = true;
        this.anim.SetFloat("moveSpeed", 0.0f);
        this.transform.LookAt(playerTransform.position);
        this.anim.SetTrigger("fire");
    }

    void CastSpell()
    {
        var spellCast = (GameObject)Instantiate(this.spellPrefab, this.eyes.position, this.eyes.rotation);
        spellCast.GetComponent<Rigidbody>().velocity = spellCast.transform.forward * this.castSpeed;
        Destroy(spellCast, 2.0f);
    }
}
