﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballController : MonoBehaviour {

    private int fireballDamage = 25;

    void OnCollisionEnter(Collision collision)
    {
        var hit = collision.gameObject;
        if (hit.tag == "dynamicObject")
        {
            Debug.Log("hit tree");
            hit.GetComponent<Animator>().SetTrigger("fall");
            Destroy(gameObject, 0.5f);
        } else if (hit.tag == "Enemy" || hit.tag == "Player")
        {
            var health = hit.GetComponent<HealthController>();
            if (health != null)
            {
                health.TakeDamage(fireballDamage);
            }
            Destroy(gameObject, 0.5f);
        }
    }
}
