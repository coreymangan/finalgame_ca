﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthController : MonoBehaviour {
    private int maxHealth;
    private int currentHealth;
    public int CurrentHealh {
        get { return this.currentHealth; }
        private set { this.currentHealth = value; }
    }

    public Slider healthBar;

	// Use this for initialization
	void Start () {
        if (this.gameObject.tag == "Player")
        {
            this.maxHealth = 200;
        } else if (this.gameObject.tag == "Enemy")
        {
            this.maxHealth = 100;
        }

        this.currentHealth = this.maxHealth;
        this.healthBar.GetComponent<Slider>().maxValue = this.maxHealth;
    }

    void Update()
    {
        this.healthBar.GetComponent<Slider>().value = this.currentHealth;
    }

    public void TakeDamage(int damage)
    {
        Debug.Log("Taking Damage");
        this.currentHealth -= damage;
        if (currentHealth <= 0)
        {
            Debug.Log("You Died");
            Destroy(this.gameObject);
        }
    }

    public void UseHealthPotion()
    {
        this.currentHealth += 50;
    }
}
