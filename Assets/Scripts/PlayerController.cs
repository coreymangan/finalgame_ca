﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour {
    public Camera cam;
    public NavMeshAgent agent;
    public Animator enemyAnim;
    public GameObject player;
    // public Transform spellSpawn;
    // public GameObject spell;

    // private float castSpeed = 12.0f;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        // Clicking where to go
		if (Input.GetMouseButtonDown(0))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                agent.SetDestination(hit.point);
            }
        }

        // Animating movement
        if (agent.remainingDistance > agent.stoppingDistance)
        {
            enemyAnim.SetFloat("moveSpeed", 1.5f);
        }
        else
        {
            enemyAnim.SetFloat("moveSpeed", 0.0f);
        }

        // Attack
        if (Input.GetMouseButtonDown(1))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                player.transform.LookAt(hit.point);
                //agent.isStopped = true;
                agent.ResetPath();
                enemyAnim.SetFloat("moveSpeed", 1.5f);
                enemyAnim.SetTrigger("fire");

                // CastSpell();
            }
        }
    }

    /*void CastSpell()
    {
        var spellCast = (GameObject)Instantiate(this.spell, this.spellSpawn.position, this.spellSpawn.rotation);
        spellCast.GetComponent<Rigidbody>().velocity = spellCast.transform.forward * this.castSpeed;
        Destroy(spellCast, 2.0f);
    }*/
}
