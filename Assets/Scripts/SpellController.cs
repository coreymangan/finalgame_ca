﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellController : MonoBehaviour {
    public GameObject spellPrefab;
    public Transform spellSpawn;
    private float castSpeed = 10.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void CastSpell()
    {
        var spellCast = (GameObject)Instantiate(this.spellPrefab, this.spellSpawn.position, this.spellSpawn.rotation);
        spellCast.GetComponent<Rigidbody>().velocity = spellCast.transform.forward * this.castSpeed;
        Destroy(spellCast, 2.0f);
    }
}
