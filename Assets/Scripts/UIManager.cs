﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {
    public GameObject pausePanel;
    public GameObject deathPanel;
    public GameObject instructionPanel;

    public Text text_hp;
    public Text text_enemies;

    private int hpLeft = 3;
    private int numEnemies;
    private int enemiesLeft;

    private bool isPaused = false;

    private HealthController hc;

    private void Start()
    {
        this.numEnemies = GameObject.FindGameObjectsWithTag("Enemy").Length;
        hc = GameObject.FindGameObjectWithTag("Player").GetComponent<HealthController>();
    }

    void Update()
    {
        this.enemiesLeft = GameObject.FindGameObjectsWithTag("Enemy").Length;
        this.text_enemies.text = "Enemies left : " + this.enemiesLeft + "/" + this.numEnemies;

        this.text_hp.text = "Health Potions left : " + this.hpLeft + "/3";

        // Pressing X will use health potion
        if (Input.GetKeyDown(KeyCode.X) && this.hpLeft > 0 && hc.CurrentHealh < 100)
        {
            this.hpLeft--;
            hc.UseHealthPotion();
        }


        // Pressing Escape will pause/unpause the game
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            this.isPaused = !this.isPaused;

            if (this.isPaused == true)
            {
                this.ResumeGame();
            }
            else if (this.isPaused == false)
            {
                this.PauseGame();
            }
        }

        // If player dies then display death panel
        if(GameObject.FindGameObjectWithTag("Player") == null)
        {
            this.PlayerDied();
        }
    }

    void PauseGame()
    {
       this.pausePanel.SetActive(true);
       Time.timeScale = 0.0f;
    }

    public void ResumeGame()
    {
        this.pausePanel.SetActive(false);
        this.instructionPanel.SetActive(false);
        Time.timeScale = 1.0f;
    }

    public void ExitGame()
    {
        Application.Quit();

        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #endif
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(0);
        this.deathPanel.SetActive(false);
    }

    void PlayerDied()
    {
        this.deathPanel.SetActive(true);
    }

    public void InstructionPanel()
    {
        this.pausePanel.SetActive(false);
        this.instructionPanel.SetActive(true);
    }

    public void CloseInstructions()
    {
        this.instructionPanel.SetActive(false);
        this.pausePanel.SetActive(true);
    }
}
